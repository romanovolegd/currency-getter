# currency-getter

## Задание:
Создать сервис, который обращается к сервису курсов валют, и отдает gif в ответ:
если курс по отношению к рублю за сегодня стал выше вчерашнего, то отдаем рандомную отсюда https://giphy.com/search/rich 
если ниже - отсюда https://giphy.com/search/broke 
Ссылки
REST API курсов валют - https://docs.openexchangerates.org/ 
REST API гифок - https://developers.giphy.com/docs/api#quick-start-guide 
Must Have
Сервис на Spring Boot 2 + Java / Kotlin
Запросы приходят на HTTP endpoint, туда передается код валюты
Для взаимодействия с внешними сервисами используется Feign
Все параметры (валюта по отношению к которой смотрится курс, адреса внешних сервисов и т.д.) вынесены в настройки
На сервис написаны тесты (для мока внешних сервисов можно использовать mockbean или WireMock)
Для сборки должен использоваться Gradle
Результатом выполнения должен быть репо на GitHub с инструкцией по запуску
Nice to Have
Сборка и запуск Docker контейнера с этим сервисом

<details><summary>Варианты запуска</summary>

- Взять самый свежий докер образ отсюда: https://gitlab.com/romanovolegd/currency-getter/container_registry/2257341  и запустить в любом удобном вам месте 

- Вообще не запускать, если просто хотите воспользоваться сервисом: http://35.228.31.182:7090/get-gif/eur
</details>

<details><summary>P.S.</summary>
Ещё есть swagger: http://35.228.31.182:7090/swagger-ui/index.html?configUrl=/api-docs/swagger-config#/
</details>


